# NK Bootstrap - JQuery

Material disponível para as aulas relacionadas à Bootstrap 4 e JQuery. Aqui será compartilhado arquivos que iremos usar para conhecer os principais componentes que essas biblíotecas dispõem para facilitar desenvolvimento de aplicações.

*BOOTSTRAP 4*: 

Framework web com códigos fontes prontos para facilitar e otimizar o processo de desenvolvimento de interfaces e front-end.
Esta é a framework mais utilizada para desenvolvedores web na construção de sites responsivos.
Seus componentes e plugins já são estruturados e prontos apenas para serem adaptados ao seu código.
Utiliza-se para aplicações que utilizam HTML, CSS e JavaScript.

*JQuery*:

A bliblioteca com funções JavaScript Jquery, é muito conhecida por estar presente nas aplicações à muito tempo. 
Seu intuito principal é simplificar os scripts que normalmente, quando feito no Vanilla JS, são mais complexos tanto na escrita quanto na leitura, assim facilitando a interpretação de quem lê o código. Além disso, trata diferenças entre os navegadores exibindo todas as características do site de forma igual independente do navegador (Google Chrome, Safari, Firefox, Opera). Também interage com HTML, tornando a UX muito mais intuítiva e agradavél, manipulando e acessando elementos direto no DOM (Document Object Model).

# Arquivos
Os arquivos do repositório são utilizados durante os treinamentos nos laboratórios da Fatec Pompéia, e os materias retirados do curso Bootstrap 4 + JQuery disponível na Udemy, sendo o instrutor: Daniel Tapias Morales.
A ideia é compartilhar o conhecimento com os estudantes do Big Data no Agronegócio para utilização em seus projetos acadêmicos.


